﻿using System;
using CryptoPredictor.Models;
using Microsoft.ML.Data;

namespace CryptoPredictor.Services
{
    public interface ICryptoService
    {
        string getDateTimeFormatted(DateTime dateTime);
        CryptoCompareGetDTO doGetLastLimitValues(string coinName, int limit);
        float computeTheNextDayValueOf(string coinName);
        MetricsDTO getMetricsAboutTheModel();
    }
}
