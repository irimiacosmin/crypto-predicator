﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using CryptoPredictor.Models;
using CryptoPredictorML.Model;
using Microsoft.ML.Data;

namespace CryptoPredictor.Services
{
    public class CryptoService : ICryptoService
    {
        private readonly string CRYPTO_COMPARE_START_PART = "https://min-api.cryptocompare.com/data/v2/histoday?fsym=";
        private readonly string CRYPTO_COMPARE_LAST_PART = "&tsym=USD&limit=10&api_key=409b2bce1e8c9f05d32323cf70ba4f1114cfdebc9106f4d596d5c05bf34617b9";
        private readonly string DATE_TIME_FORMAT = "MM/dd/yyyy";

        public string getDateTimeFormatted(DateTime dateTime)
        {
            return dateTime.ToString(DATE_TIME_FORMAT);
        }

        public float computeTheNextDayValueOf(string coinName)
        {
            CryptoCompareGetDTO cryptoCompareGet = doGetLastLimitValues(coinName, 10);
            if (!cryptoCompareGet.Response.Equals("Success"))
            {
                return 0f;
            }

            List<CoinExchangeDTO> coinExchangeDTOs = cryptoCompareGet.Data?.Data ?? new List<CoinExchangeDTO>();
            float lastTenOpen = (float) coinExchangeDTOs.Select(x => x.open).Average();
            float lastTenHigh = (float) coinExchangeDTOs.Select(x => x.high).Average();
            float lastTenLow = (float) coinExchangeDTOs.Select(x => x.low).Average();
            
            var input = new ModelInput
            {
                Date = DateTime.Today.ToString(DATE_TIME_FORMAT),
                Open = lastTenOpen,
                Low = lastTenLow,
                High = lastTenHigh,
                Coin = coinName

            };
            ModelOutput result = ConsumeModel.Predict(input);
            return result.Score;
        }

        public MetricsDTO getMetricsAboutTheModel()
        {
            return new MetricsDTO();
        }

        public CryptoCompareGetDTO doGetLastLimitValues(string coinName, int limit)
        {
            Uri lastTenUri = new Uri(CRYPTO_COMPARE_START_PART + coinName + CRYPTO_COMPARE_LAST_PART);
            string response = doGet(lastTenUri);
            CryptoCompareGetDTO cryptoCompareGetDto = Newtonsoft.Json.JsonConvert.DeserializeObject<CryptoCompareGetDTO>(response);
            return cryptoCompareGetDto;
        }

        private string doGet(Uri uri)
        {
            using var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate })
            {
                BaseAddress = uri
            };
            HttpResponseMessage response = client.GetAsync(uri).Result;
            response.EnsureSuccessStatusCode();
            return response.Content.ReadAsStringAsync().Result;
        }
    }
}
