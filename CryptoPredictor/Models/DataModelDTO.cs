﻿using System.Collections.Generic;

namespace CryptoPredictor.Models
{
    public class DataModelDTO
    {
        public bool Aggregated { get; set; }
        public long TimeFrom { get; set; }
        public long TimeTo { get; set; }
        public List<CoinExchangeDTO> Data { get; set; }
    }
}
