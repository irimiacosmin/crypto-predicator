﻿namespace CryptoPredictor.Models
{
    public class CryptoCompareGetDTO
    { 
        public string Response { get; set; }
        public string Message { get; set; }
        public bool HasWarning { get; set; }
        public int Type { get; set; }
        public DataModelDTO Data { get; set; }
    }
}
