﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoPredictor.Models
{
    public class MetricsDTO
    {
        public double MeanAbsoluteError { get; set; }
        public double MeanSquaredError { get; set; }
        public double RootMeanSquaredError { get; set; }
        public double LossFunction { get; set; }
        public double RSquared { get; set; }

        public MetricsDTO()
        {
            MeanAbsoluteError = 5.417313291692829;
            MeanSquaredError = 970.0583125340038;
            RootMeanSquaredError = 31.14575914204057;
            LossFunction = 970.0583132333834;
            RSquared = 0.9995350843534974;
        }
    }
}
