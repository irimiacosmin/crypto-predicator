﻿using Microsoft.ML.Data;

namespace CryptoPredictor.Models
{
    public class CryptoOutputDTO
    {
        public float CoinPrice { get; set; }
        public string Date { get; set; }
        public string Currency { get; set; }
        public MetricsDTO DetailsAboutModel { get; set; }
    }
}
