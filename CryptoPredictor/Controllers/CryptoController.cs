﻿using System;
using CryptoPredictor.Models;
using CryptoPredictor.Services;
using Microsoft.AspNetCore.Mvc;

namespace CryptoPredictor.Controllers
{
    [ApiController]
    [Route("crypto")]
    public class CryptoController : ControllerBase
    {
        private readonly ICryptoService cryptoService;

        public CryptoController(ICryptoService _cryptoService)
        {
            cryptoService = _cryptoService;
        }

        [Route("tomorrowPrice/{coinName}")]
        [HttpGet]
        public CryptoOutputDTO GetBy(string coinName)
        {
            float val = cryptoService.computeTheNextDayValueOf(coinName);
            string dateFormatted = cryptoService.getDateTimeFormatted(DateTime.Now.AddDays(1));
            MetricsDTO metrics = cryptoService.getMetricsAboutTheModel();
            return new CryptoOutputDTO
            {
                CoinPrice = val,
                Date = dateFormatted,
                Currency = "USD",
                DetailsAboutModel = metrics
            };
        }
    }
}
